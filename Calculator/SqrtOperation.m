//
//  SqrtOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "SqrtOperation.h"

@implementation SqrtOperation

-(double)perform:(double)value
{
    return sqrt(value);
}
@end
