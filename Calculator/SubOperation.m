//
//  SubOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "SubOperation.h"

@implementation SubOperation
// вычитание
-(double)performWithSecondArgument:(double)secondArgument
{
    return self.firstArgument-secondArgument;
}
@end
