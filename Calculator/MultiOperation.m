//
//  MultiOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "MultiOperation.h"

@implementation MultiOperation

-(double)performWithSecondArgument:(double)secondArgument
{
    return self.firstArgument*secondArgument;
}
@end
