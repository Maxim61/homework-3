//
//  SignOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "SignOperation.h"

@implementation SignOperation
-(double)perform:(double)value
{
    return -value;
}

@end
