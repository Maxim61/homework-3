//
//  ViewController.h
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"
@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *NumericLabel;

@property(nonatomic,assign,getter=isNumberInputing)BOOL numberInputing;

@property(nonatomic,strong)CalculatorModel *model;

@property(nonatomic,strong)NSString* lastOperation;

-(void)ProverkaLastOperation:(UIButton *)sender : (double)znachenii: (NSString*)operationString;
@end

