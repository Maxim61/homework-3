//
//  BinaryOperation.h
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BinaryOperation : NSObject

-(double)performWithSecondArgument:(double)secondArgument;

@property (nonatomic,assign) double firstArgument;


@end
