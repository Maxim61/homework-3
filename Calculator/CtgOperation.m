//
//  CtgOperation.m
//  Calculator
//
//  Created by Евгений Гуляев on 11.11.16.
//  Copyright © 2016 Евгений Гуляев. All rights reserved.
//

#import "CtgOperation.h"

@implementation CtgOperation

-(double)perform:(double)value
{
    return (1/tan(value*M_PI/180));
}
@end
