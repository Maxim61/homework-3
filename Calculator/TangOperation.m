//
//  TangOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "TangOperation.h"

@implementation TangOperation

-(double)perform:(double)value
{
    return tan(value*M_PI/180);
}
@end
