//
//  SumOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "SumOperation.h"

@implementation SumOperation
-(double)performWithSecondArgument:(double)secondArgument
{
    return self.firstArgument+secondArgument;
}

@end
