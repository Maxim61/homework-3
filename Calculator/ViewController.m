//
//  ViewController.m
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController


- (IBAction)pressDigit:(UIButton *)sender {
    _lastOperation=sender.currentTitle;
    if(self.isNumberInputing)
    {
        if (![sender.currentTitle isEqualToString:@","]
            || ![self.NumericLabel.text containsString:@","])
        {
            self.NumericLabel.text=[self.NumericLabel.text stringByAppendingString :sender.currentTitle];
        }
    }
    else
    {
        if([sender.currentTitle isEqualToString:@","])
        {
            self.NumericLabel.text=[NSString stringWithFormat:@"0,"];
        }
        else
        {
            self.NumericLabel.text=sender.currentTitle;
        }
        self.numberInputing=YES;
    }
}

- (IBAction)performOperation:(UIButton *)sender {
 
    self.numberInputing = NO;
    double value =[self.NumericLabel.text stringByReplacingOccurrencesOfString:@"," withString:@"."].doubleValue;
    NSString *operationString = sender.currentTitle;
    [self ProverkaLastOperation:sender:value :operationString];
}


-(void)viewDidLoad
{
    [super viewDidLoad];
    self.model=[[CalculatorModel alloc]init];
}

-(void)ProverkaLastOperation:(UIButton *)sender  :(double)znachenii :(NSString *)operationString
{
    if ((sender.currentTitle!=_lastOperation) ||
        (  ![sender.currentTitle isEqual: @"-"] &&![sender.currentTitle isEqual: @"+"] &&
           ![sender.currentTitle isEqual: @"/"] &&![sender.currentTitle isEqual: @"*"] &&
           ![sender.currentTitle isEqual: @"="]
       )
       )
  {
      self.NumericLabel.text= [[@([self.model performOperation:operationString withValue:znachenii])
                                    stringValue]
                                    stringByReplacingOccurrencesOfString:@"." withString:@","];
      _lastOperation = operationString;
  }
}

@end
