//
//  SinOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "SinOperation.h"

@implementation SinOperation

-(double)perform:(double)value
{
    return sin(value*M_PI/180);
}
@end
