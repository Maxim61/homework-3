//
//  CalculatorModel.m
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "CalculatorModel.h"
#import "SumOperation.h"
#import "EqualOperation.h"
#import "SignOperation.h"
#import "SinOperation.h"
#import "SqrtOperation.h"
#import "SubOperation.h"
#import "DivisionOperation.h"
#import "MultiOperation.h"
#import "LNOperation.h"
#import "TangOperation.h"
#import "Procent.h"
#import "CosOperation.h"
#import "CrtDisplay.h"




@implementation CalculatorModel

-(instancetype)init
{
    self=[super init];
    if (self) {
        _operations=@
        {
            @"%":[[Procent alloc]init],
            @"C":[[CrtDisplay alloc]init],
            @"sin":[[SinOperation alloc]init],
            @"√":[[SqrtOperation alloc]init],
            @"/":[[DivisionOperation alloc]init],
            @"*":[[MultiOperation alloc]init],
            @"ln":[[LNOperation alloc]init],
            @"-":[[SubOperation alloc]init],
            @"+":[[SumOperation alloc]init],
            @"±":[[SignOperation alloc]init],
            @"=":[[EqualOperation alloc]init],
            @"cos":[[CosOperation alloc]init],
            @"tan":[[TangOperation alloc]init],
        };
        
    }
    return self;
}

-(id)operationForOperationString:(NSString *)operationString
{
    id operation=[self.operations objectForKey:operationString];
    if (!operation)
    {
        operation=[[UnaryOperation alloc]init];
    }
    return operation;
}
-(double)performOperation:(NSString *)operationString withValue:(double)value
{
    id operation=[self operationForOperationString:operationString];
    
    if([operation isKindOfClass:[EqualOperation class]])
    {
        if (self.currentBinaryOperation)
        {
            value=[self.currentBinaryOperation performWithSecondArgument:value];
        }
    }
    if ([operation isKindOfClass:[UnaryOperation class]])
    {
        return [operation perform:value];
    }
    if([operation isKindOfClass:[BinaryOperation class]])
    {
        self.currentBinaryOperation=operation;
        self.currentBinaryOperation.firstArgument=value;
        return value;
    }
    return DBL_MAX;
}

@end
