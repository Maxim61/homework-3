//
//  LNOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "LNOperation.h"

@implementation LNOperation

-(double)perform:(double)value
{
    return log(value);
}

@end
