//
//  UnaryOperation.h
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnaryOperation : NSObject

-(double)perform:(double)value;

@end
