//
//  CalculatorModel.h
//  Calculator
//
//  Created by Максим Гуляев on 10.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnaryOperation.h"
#import "BinaryOperation.h"

@interface CalculatorModel : NSObject

@property(nonatomic,strong) NSDictionary *operations;

@property(nonatomic,strong) BinaryOperation *currentBinaryOperation;

-(double)performOperation:(NSString *)operationString withValue:(double)value;

@end
