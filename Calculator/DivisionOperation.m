//
//  DivisionOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "DivisionOperation.h"

@implementation DivisionOperation

-(double)performWithSecondArgument:(double)secondArgument
{
    return self.firstArgument/secondArgument;
}
@end
