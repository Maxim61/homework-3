//
//  CosOperation.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "CosOperation.h"

@implementation CosOperation

-(double)perform:(double)value
{
    return cos(value*M_PI/180);
}

@end
