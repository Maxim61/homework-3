//
//  Procent.m
//  Calculator
//
//  Created by Максим Гуляев on 11.11.16.
//  Copyright © 2016 Максим Гуляев. All rights reserved.
//

#import "Procent.h"

@implementation Procent

-(double)perform:(double)value
{
    return value/100;
}
@end
